=================
Installing pyDARM
=================

.. _pydarm-install-conda:

-----
Conda
-----

The recommended way of installing pyDARM is with `Conda <https://conda.io>`__:

.. code-block:: bash

   conda install -c conda-forge pydarm


.. _pydarm-install-pip:

---
Pip
---

.. code-block:: bash

    python -m pip install pydarm
